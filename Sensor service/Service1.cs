﻿using Microsoft.AspNet.SignalR.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace Sensor_service
{
    public partial class Service1 : ServiceBase
    {
        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(SetConsoleCtrlEventHandler handler, bool add);
        private delegate bool SetConsoleCtrlEventHandler(CtrlType sig);
        private enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }
        //TODO: change these constants to match your setup
        private static MqttClient MqttClient = null;
        private static mscl.Connection connection = null;
        private static mscl.Connection connection2 = null;
        private static mscl.BaseStation basestation2 = null;
        private static MongoClient clientMongo = null;
        private static IMongoDatabase database = null;
        private static string IPAddress = "";
        private static ushort Port = 0;
        int interv = 3000;
        List<string> SpecialNode = new List<string>();
        Dictionary<string, List<decimal>> listNoiseVal = new Dictionary<string, List<decimal>>();
        Dictionary<string, DateTime> listNoiseTime = new Dictionary<string, DateTime>();

        static HubConnection connectionSignalR = null;
        IHubProxy appHub = null;
        private string hubUrl = ConfigurationSettings.AppSettings["hubUrl"].ToString();

        private static bool Handler(CtrlType signal)
        {
            switch (signal)
            {
                case CtrlType.CTRL_BREAK_EVENT:
                case CtrlType.CTRL_C_EVENT:
                case CtrlType.CTRL_LOGOFF_EVENT:
                case CtrlType.CTRL_SHUTDOWN_EVENT:
                case CtrlType.CTRL_CLOSE_EVENT:
                    connection.disconnect();
                    MqttClient.Disconnect();
                    //Console.WriteLine("Closing");
                    // TODO Cleanup resources
                    Environment.Exit(0);
                    return false;

                default:
                    return false;
            }
        }
        public Service1()
        {
            InitializeComponent();
        }
        public void OnDebug()
        {
            OnStart(null);
        }
        protected override void OnStart(string[] args)
        {
            string IPAddress2 = "";
            ushort Port2 = 0;
            string MQTTHost = ConfigurationManager.AppSettings["MQTTHost"].ToString();
            string mongoDBsvr = ConfigurationManager.AppSettings["mongodb_server"].ToString();
            string basepatch = AppDomain.CurrentDomain.BaseDirectory + "AppSetting.json";
            SpecialNode.Add("25293");
            SpecialNode.Add("27878");
            SpecialNode.Add("27879");
            SpecialNode.Add("27880");
            SpecialNode.Add("27881");
            SpecialNode.Add("27882");

            listNoiseVal.Add("25293", new List<decimal>());
            listNoiseVal.Add("27878", new List<decimal>());
            listNoiseVal.Add("27879", new List<decimal>());
            listNoiseVal.Add("27880", new List<decimal>());
            listNoiseVal.Add("27881", new List<decimal>());
            listNoiseVal.Add("27882", new List<decimal>());

            listNoiseTime.Add("25293", new DateTime());
            listNoiseTime.Add("27878", new DateTime());
            listNoiseTime.Add("27879", new DateTime());
            listNoiseTime.Add("27880", new DateTime());
            listNoiseTime.Add("27881", new DateTime());
            listNoiseTime.Add("27882", new DateTime());

            // mongoDB connection 
            clientMongo = new MongoClient(mongoDBsvr);
            database = clientMongo.GetDatabase("IoT");

            if (File.Exists(basepatch))
            {
                string Json = File.ReadAllText(basepatch);
                if (Json != "null")
                {
                    object rootresult = JsonConvert.DeserializeObject(Json);
                    JObject rootobj = JObject.Parse(rootresult.ToString());
                    string poIPAddress = rootobj["IPAddress"].ToString();
                    string poPort = rootobj["Port"].ToString();
                    string poIPAddress2 = rootobj["IPAddress2"].ToString();
                    string poPort2 = rootobj["Port2"].ToString();
                    string poMQTT = rootobj["MQTTHOST"].ToString();
                    interv = Convert.ToInt32(rootobj["Interval"].ToString());
                    hubUrl = rootobj["hubUrl"].ToString();
                    MQTTHost = poMQTT;
                    IPAddress = poIPAddress;
                    IPAddress2 = poIPAddress2;
                    Port = Convert.ToUInt16(poPort);
                    Port2 = Convert.ToUInt16(poPort2);
                }
            }

            initRTHub();

            // mqtt connection 
            MqttClient = new MqttClient(MQTTHost);
            MqttClient.MqttMsgPublishReceived += client_MqttMsgPublishReceived;
            MqttClient.Subscribe(new string[] { "Sensor/Lord/#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
            string clientId = Guid.NewGuid().ToString();
            MqttClient.Connect(clientId);
            void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
            {
                var message = System.Text.Encoding.Default.GetString(e.Message);
                if (IsValidJson(message))
                {
                    string[] topic = e.Topic.Split('/');
                    string Category = topic[0];
                    string hostDevice = topic[1];
                    if (topic.Length > 1)
                    {
                        string cmd = topic[2];
                        switch (cmd)
                        {
                            case "SetConfig":
                                setConfig(message);
                                break;
                            case "GetConfig":
                                getConfig(message);
                                break;
                            default:

                                break;
                        }
                    }
                }
            }

            connection = mscl.Connection.TcpIp(IPAddress, Port);
            Thread b1 = new Thread(new ThreadStart(HandlerBasestationOne));
            Thread s = new Thread(new ThreadStart(NoiseHandler));
            b1.Start();
            s.Start();

            //connection2 = mscl.Connection.TcpIp(IPAddress2, Port2);
            //Create basestation
            //basestation2 = new mscl.BaseStation(connection2);
            //Thread b2 = new Thread(new ThreadStart(HandlerBasestationTwo));
            //b2.Start();
        }
        private void initRTHub()
        {
            try
            {
                //Set connection
                connectionSignalR = new HubConnection(hubUrl);
                //Make proxy to hub based on hub name on server
                appHub = connectionSignalR.CreateHubProxy("apphub");
                //Start connection

                connectionSignalR.Start().ContinueWith(task => {
                    if (task.IsFaulted)
                    {
                        task.Exception.GetBaseException();
                    }
                    else
                    {
                        Console.WriteLine("Connected");
                    }
                }).Wait();
            }
            catch (Exception ex)
            {

            }
        }
        private void heandlerSignalR(string mod, string tp, string sp)
        {
            try
            {
                appHub.Invoke<string>("Send", mod, tp, sp).ContinueWith(task => {
                    if (task.IsFaulted)
                    {
                        Console.WriteLine("There was an error calling send: {0}",
                                          task.Exception.GetBaseException());
                        if (connectionSignalR.State == Microsoft.AspNet.SignalR.Client.ConnectionState.Disconnected)
                        {
                            initRTHub();
                            heandlerSignalR(mod, tp, sp);
                        }

                    }
                    else
                    {
                        Console.WriteLine(task.Result);
                    }
                });
            }
            catch (Exception ex)
            {
                if (connectionSignalR.State == Microsoft.AspNet.SignalR.Client.ConnectionState.Disconnected)
                {
                    initRTHub();
                    heandlerSignalR(mod, tp, sp);
                }
            }
        }
        private void HandlerSpecialNode()
        {
            try
            {
                mscl.BaseStation basestations = new mscl.BaseStation(connection);
                string specialNode = "";
                string specialChanel = "";
                DateTime poDateTime = new DateTime();
                List<decimal> poVal = new List<decimal>();
                var DataDevice = database.GetCollection<Sensor_REC>("SensorDataDevice");
                var haystackPoint = database.GetCollection<HaystackPointView_REC>("HaystackPoint");
                Thread.Sleep(2000);
                while (true)
                {
                    mscl.DataSweeps sweeps = basestations.getData(1000);
                    foreach (mscl.DataSweep sweep in sweeps)
                    {
                        sweep.nodeAddress();    //the node address the sweep is from
                        sweep.timestamp();      //the TimeStamp of the sweep
                        sweep.tick();           //the tick of the sweep (0 - 65535 counter)
                        sweep.sampleRate();     //the sample rate of the sweep
                        sweep.samplingType();   //the SamplingType of the sweep (sync, nonsync, burst, etc.)
                        sweep.nodeRssi();       //the signal strength at the Node
                        sweep.baseRssi();       //the signal strength at the BaseStation
                        sweep.frequency();      //the radio frequency this was collected on
                        //get the vector of data in the sweep
                        mscl.ChannelData data = sweep.data();

                        string nodeAddress = sweep.nodeAddress().ToString();
                        if (SpecialNode.IndexOf(nodeAddress) >= 0) // special node
                        {
                            int i = 0;
                            foreach (mscl.WirelessDataPoint dataPoint in data)
                            {
                                Dictionary<string, string> ResDataObj = new Dictionary<string, string>();

                                dataPoint.storedAs();
                                poDateTime = DateTime.Now;
                                specialNode = nodeAddress;
                                specialChanel = dataPoint.channelName();
                                poVal.Add(Convert.ToDecimal(dataPoint.as_string()));
                                i++;
                            }
                        }

                        if (poVal.Count > 0)
                        {
                            if ((DateTime.Now - poDateTime).TotalSeconds > 10)
                            {
                                Dictionary<int, Dictionary<string, string>> ResData = new Dictionary<int, Dictionary<string, string>>();

                                List<decimal> peakList = new List<decimal>();
                                Dictionary<string, string> ResDataObj = new Dictionary<string, string>();
                                for (int i = 0; i < poVal.Count; i++) // collect peak data
                                {
                                    decimal poPrevious = (i == 0) ? Convert.ToDecimal("0.00") : poVal[i - 1];
                                    decimal poCurrent = poVal[i];
                                    decimal poNext = (i + 1 == poVal.Count) ? poCurrent : poVal[i + 1];
                                    if (poCurrent > poPrevious && poCurrent > poNext)
                                    {
                                        peakList.Add(poCurrent);
                                    }
                                }
                                decimal peakData = Convert.ToDecimal("0.00");
                                for (int a = 0; a < peakList.Count; a++) // sum peak data
                                {
                                    peakData += peakList[a];
                                }
                                peakData = peakData / peakList.Count;
                                peakData = Math.Round(peakData, 5);
                                //Console.Write("Address special node :" + specialNode + " | " + (DateTime.Now - poDateTime).TotalSeconds + " | Chanel : " + specialChanel + " value ( " + peakData + ") \n");

                                // begin insert and update logs
                                Sensor_REC voSensor = new Sensor_REC();
                                voSensor.RecordTimestamp = DateTime.Now;
                                voSensor.DeviceID = basestations.name();
                                voSensor.Node = specialNode;
                                voSensor.Chanel = specialChanel;
                                voSensor.PresentValue = peakData.ToString();

                                List<Dictionary<string, string>> stl = new List<Dictionary<string, string>>();
                                Dictionary<string, string> st = new Dictionary<string, string>();
                                var builder = Builders<Sensor_REC>.Filter;
                                var filter = builder.And(builder.Eq<string>("Node", voSensor.Node), builder.Eq<string>("Chanel", voSensor.Chanel));
                                var resDatas = DataDevice.Find(filter).SingleOrDefault();
                                if (resDatas == null)
                                {
                                    DataDevice.InsertOne(voSensor);
                                    st.Add("id", voSensor.Node + "-" + voSensor.Chanel);
                                    st.Add("device", "");
                                    st.Add("max", "");
                                    st.Add("min", "");
                                }
                                else
                                {
                                    var update = Builders<Sensor_REC>.Update.Set("PresentValue", voSensor.PresentValue);
                                    DataDevice.UpdateOne(filter, update);
                                    st.Add("id", voSensor.Node + "-" + voSensor.Chanel + "-" + resDatas.Zone);
                                    st.Add("device", resDatas.DeviceName);
                                    st.Add("max", resDatas.Max.ToString());
                                    st.Add("min", resDatas.Min.ToString());
                                }
                                // end insert and update logs
                                st.Add("value", voSensor.PresentValue);
                                stl.Add(st);

                                // publish to mqtt
                                string strValue = Convert.ToString(JsonConvert.SerializeObject(stl));
                                MqttClient.Publish("WidgetRealtime/SensorWidget", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);

                                ResDataObj.Add("ChanelName", specialChanel);
                                ResDataObj.Add("Value", peakData.ToString());
                                ResData.Add(0, ResDataObj);

                                poVal.Clear();
                                specialNode = "";
                                specialChanel = "";
                            }
                        }
                    }
                    Thread.Sleep(interv);
                }
            }
            catch (mscl.Error e)
            {
                SendMail("the service sensor reported", e.Message);
                Dictionary<string, string> ResDataObj = new Dictionary<string, string>();
                ResDataObj.Add("msg", e.Message);
                string strValue = Convert.ToString(JsonConvert.SerializeObject(ResDataObj));
                MqttClient.Publish("Sensor/Lord/All/Error", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
        }
        private void HandlerBasestationOne()
        {
            try
            {
                mscl.BaseStation basestation = new mscl.BaseStation(connection);
                string specialNode = "";
                string specialChanel = "";

                DateTime poDateTime = new DateTime();
                int restartTime = 0;
                List<decimal> poVal = new List<decimal>();
                var DataDevice = database.GetCollection<Sensor_REC>("SensorDataDevice");
                var DataLogs = database.GetCollection<SensorDatalogs_REC>("SensorDataLogs");
                var haystackPoint = database.GetCollection<HaystackPointView_REC>("HaystackPoint");
                var constantVal = database.GetCollection<ConfigurationModel>("Configuration");
                Thread.Sleep(2000);
                while (true)
                {
                    //get all the data sweeps that have been collected, with a timeout of 500 milliseconds
                    mscl.DataSweeps sweeps = basestation.getData(1000);
                    if (sweeps.Count == 0)
                    {
                        connection.reconnect();
                        if (restartTime >= 10)
                        {
                            SendMail("the service sensor reported", "the service sensor base station (" + IPAddress + ":" + Port + ") restarted");
                            // Restart Apps
                            Process currentProcess = Process.GetCurrentProcess();
                            int pid = currentProcess.Id;
                            string applicationName = currentProcess.ProcessName;
                            RestartApp(pid, applicationName);
                            System.Environment.Exit(1);
                        }
                        else if (restartTime == 1)
                        {
                            SendMail("the service sensor reported", "can't read data or service disconnected from the base station (" + IPAddress + ":" + Port + ")");
                        }
                        restartTime++;
                    }
                    else
                    {
                        restartTime = 0;
                        foreach (mscl.DataSweep sweep in sweeps)
                        {
                            sweep.nodeAddress();    //the node address the sweep is from
                            sweep.timestamp();      //the TimeStamp of the sweep
                            sweep.tick();           //the tick of the sweep (0 - 65535 counter)
                            sweep.sampleRate();     //the sample rate of the sweep
                            sweep.samplingType();   //the SamplingType of the sweep (sync, nonsync, burst, etc.)
                            sweep.nodeRssi();       //the signal strength at the Node
                            sweep.baseRssi();       //the signal strength at the BaseStation
                            sweep.frequency();      //the radio frequency this was collected on

                            //get the vector of data in the sweep
                            mscl.ChannelData data = sweep.data();
                            //iterate over each point in the sweep (one point per channel)
                            string nodeAddress = sweep.nodeAddress().ToString();
                            if (SpecialNode.IndexOf(nodeAddress) >= 0)
                            {
                                int i = 0;
                                foreach (mscl.WirelessDataPoint dataPoint in data)
                                {
                                    dataPoint.storedAs();
                                    poDateTime = DateTime.Now;
                                    specialNode = nodeAddress;
                                    specialChanel = dataPoint.channelName();
                                    poVal.Add(Convert.ToDecimal(dataPoint.as_string()));
                                    if (dataPoint.channelName() == "ch1")
                                    {
                                        listNoiseTime[nodeAddress] = DateTime.Now;
                                        listNoiseVal[nodeAddress].Add(Convert.ToDecimal(dataPoint.as_string()));
                                    }
                                    i++;
                                }
                            }
                            else // general node
                            {
                                Dictionary<int, Dictionary<string, string>> ResData = new Dictionary<int, Dictionary<string, string>>();
                                int i = 0;
                                foreach (mscl.WirelessDataPoint dataPoint in data)
                                {
                                    List<Dictionary<string, string>> stl = new List<Dictionary<string, string>>();
                                    Dictionary<string, string> ResDataObj = new Dictionary<string, string>();
                                    //dataPoint.channelName();    //the name of the channel for this point
                                    //dataPoint.storedAs();       //the ValueType that the data is stored as
                                    //dataPoint.as_float();       //get the value as a float
                                    //Console.Write("Address node :" + sweep.nodeAddress() + " | Chanel : " + dataPoint.channelName() + " value ( ");
                                    dataPoint.storedAs();

                                    // begin insert and update logs
                                    Sensor_REC voSensor = new Sensor_REC();
                                    voSensor.RecordTimestamp = DateTime.Now;
                                    voSensor.DeviceID = basestation.name();
                                    voSensor.Node = sweep.nodeAddress().ToString();
                                    voSensor.Chanel = dataPoint.channelName();
                                    voSensor.PresentValue = dataPoint.as_string();

                                    SensorDatalogs_REC vologs = new SensorDatalogs_REC();
                                    vologs.RecordTimestamp = DateTime.Now;
                                    vologs.DeviceID = basestation.name();
                                    vologs.Node = sweep.nodeAddress().ToString();
                                    vologs.Chanel = dataPoint.channelName();
                                    vologs.PresentValue = dataPoint.as_string();

                                    if (voSensor.Chanel.Length <= 4)
                                    {
                                        Dictionary<string, string> st = new Dictionary<string, string>();
                                        var builder = Builders<Sensor_REC>.Filter;
                                        var filter = builder.And(builder.Eq<string>("Node", voSensor.Node), builder.Eq<string>("Chanel", voSensor.Chanel));
                                        var resDatas = DataDevice.Find(filter).SingleOrDefault();
                                        if (resDatas == null)
                                        {
                                            DataDevice.InsertOne(voSensor);
                                            st.Add("id", voSensor.Node + "-" + voSensor.Chanel);
                                            st.Add("device", "");
                                            st.Add("max", "");
                                            st.Add("min", "");
                                        }
                                        else
                                        {
                                            var update = Builders<Sensor_REC>.Update.Set("PresentValue", voSensor.PresentValue);
                                            DataDevice.UpdateOne(filter, update);
                                            st.Add("id", voSensor.Node + "-" + voSensor.Chanel + "-" + resDatas.Zone);
                                            st.Add("device", resDatas.DeviceName);
                                            st.Add("max", resDatas.Max.ToString());
                                            st.Add("min", resDatas.Min.ToString());
                                        }
                                        // end insert and update logs
                                        st.Add("value", voSensor.PresentValue);
                                        stl.Add(st);
                                        // publish to mqtt
                                        string strValue = Convert.ToString(JsonConvert.SerializeObject(stl));
                                        heandlerSignalR("WidgetRealtime", "SensorWidget", strValue);
                                        //MqttClient.Publish("WidgetRealtime/SensorWidget", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                                        if (voSensor.Node == "25001")
                                        {
                                            if (voSensor.Chanel == "ch7")
                                            {
                                                Dictionary<string, string> sts1 = new Dictionary<string, string>();
                                                sts1.Add("value", voSensor.PresentValue);
                                                string strValue1 = Convert.ToString(JsonConvert.SerializeObject(sts1));
                                                heandlerSignalR("WidgetRealtime", "SensortempData", strValue1);
                                                //MqttClient.Publish("WidgetRealtime/SensortempData", Encoding.UTF8.GetBytes(strValue1), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);

                                                //check treshold
                                                var consFilter = new BsonDocument() { };
                                                ConfigurationModel resConsVal = constantVal.Find(consFilter).Limit(1).SingleOrDefault();
                                                if (resConsVal != null)
                                                {
                                                    if (Convert.ToDecimal(voSensor.PresentValue) > resConsVal.OutDoorTemperature && resConsVal.OutDoorTemperatureStatus == true)
                                                    {
                                                        object msgCurtainRight = new
                                                        {
                                                            deviceID = "127001",
                                                            ObjectType = "OBJECT_BINARY_VALUE:4102",
                                                            value = "1",
                                                        };
                                                        MqttClient.Publish("Curtain/Bacnet/Write", Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(msgCurtainRight)), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                                                    }
                                                }
                                            }
                                            else if (voSensor.Chanel == "ch8")
                                            {
                                                Dictionary<string, string> sts2 = new Dictionary<string, string>();
                                                sts2.Add("value", voSensor.PresentValue);
                                                string strValue2 = Convert.ToString(JsonConvert.SerializeObject(sts2));
                                                heandlerSignalR("WidgetRealtime", "SensorhumidityData", strValue2);
                                                //MqttClient.Publish("WidgetRealtime/SensorhumidityData", Encoding.UTF8.GetBytes(strValue2), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                                            }
                                        }
                                        else if (voSensor.Node == "25003")
                                        {
                                            if (voSensor.Chanel == "ch7" || voSensor.Chanel == "ch8")
                                            {
                                                Dictionary<string, string> sts1 = new Dictionary<string, string>();
                                                sts1.Add("value", voSensor.PresentValue);
                                                sts1.Add("chanel", voSensor.Chanel);
                                                string strValue1 = Convert.ToString(JsonConvert.SerializeObject(sts1));
                                                MqttClient.Publish("SensorMobile/Partnership/Read", Encoding.UTF8.GetBytes(strValue1), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);


                                            }
                                        }
                                        DataLogs.InsertOne(vologs); // insert Logs
                                        ResDataObj.Add("ChanelName", dataPoint.channelName());
                                        ResDataObj.Add("Value", dataPoint.as_string());
                                        ResData.Add(i, ResDataObj);
                                        i++;
                                        if (voSensor.Node != "4660")
                                        {
                                            var bldLg = Builders<HaystackPointView_REC>.Filter;
                                            //var fltLg = bldLg.And(bldLg.Eq<string>("DeviceID", voSensor.Node), bldLg.Eq<string>("channelID", voSensor.Chanel));
                                            string voID = "@" + voSensor.Node + "_" + voSensor.Chanel;
                                            var fltLg = bldLg.Eq<string>("id", voID);
                                            var updatelg = Builders<HaystackPointView_REC>.Update.Set("curVal", voSensor.PresentValue.ToString());
                                            haystackPoint.UpdateOne(fltLg, updatelg);
                                        }
                                    }
                                }

                                //var bd = Builders<Sensor_REC>.Filter.Empty;
                                //var res = DataDevice.Find(bd).ToList();
                                //Dictionary<string, string> sts = new Dictionary<string, string>();
                                //sts.Add("id", "Total");
                                //sts.Add("value", res.Count.ToString());
                                //stl.Add(sts);
                            }


                        }
                    }
                    Thread.Sleep(interv);
                }
            }
            catch (mscl.Error e)
            {
                SendMail("the service sensor reported", e.Message);
                Dictionary<string, string> ResDataObj = new Dictionary<string, string>();
                ResDataObj.Add("msg", e.Message);
                string strValue = Convert.ToString(JsonConvert.SerializeObject(ResDataObj));
                heandlerSignalR("WidgetRealtime", "SensorError", strValue);
                MqttClient.Publish("Sensor/Lord/All/Error", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
        }

        private void NoiseHandler()
        {
            var DataDevice = database.GetCollection<Sensor_REC>("SensorDataDevice");
            var DataLogs = database.GetCollection<SensorDatalogs_REC>("SensorDataLogs");
            while (true)
            {
                foreach (string Node in SpecialNode)
                {
                    // noise sensor
                    if (listNoiseVal[Node].Count > 0)
                    {
                        if ((DateTime.Now - listNoiseTime[Node]).TotalSeconds > 10)
                        {
                            Dictionary<int, Dictionary<string, string>> ResData = new Dictionary<int, Dictionary<string, string>>();

                            List<decimal> peakList = new List<decimal>();
                            Dictionary<string, string> ResDataObj = new Dictionary<string, string>();
                            decimal min = listNoiseVal[Node].Min();
                            decimal max = listNoiseVal[Node].Max();
                            decimal mid = (max + min) / 2;
                            mid = Math.Abs(mid);
                            for (int i = 0; i < listNoiseVal[Node].Count; i++) // collect peak data
                            {
                                decimal poPrevious = (i == 0) ? Convert.ToDecimal("0.00") : listNoiseVal[Node][i - 1] + mid;
                                decimal poCurrent = listNoiseVal[Node][i] + mid;
                                decimal poNext = (i + 1 == listNoiseVal[Node].Count) ? poCurrent : listNoiseVal[Node][i + 1] + mid;
                                if (poCurrent > poPrevious && poCurrent > poNext)
                                {
                                    peakList.Add(poCurrent);
                                }
                            }
                            decimal peakData = Convert.ToDecimal("0.00");
                            decimal pa = Convert.ToDecimal("0.00002");
                            for (int a = 0; a < peakList.Count; a++) // sum peak data
                            {
                                peakData += peakList[a];
                            }
                            decimal poPP = peakData / peakList.Count;
                            peakData = peakData / peakList.Count;
                            peakData = Math.Round(peakData, 5);
                            double voPeak = Convert.ToDouble(peakData) / Convert.ToDouble(pa);
                            double voPeakData = Math.Log10(voPeak);
                            double poPeakData = Math.Pow(voPeakData, 2);

                            if (poPP < 0) { poPeakData = 0; }


                            // begin insert and update logs
                            Sensor_REC voSensor = new Sensor_REC();
                            voSensor.RecordTimestamp = DateTime.Now;
                            voSensor.DeviceID = "";
                            voSensor.Node = Node;
                            voSensor.Chanel = "ch1";
                            voSensor.PresentValue = poPeakData.ToString();

                            SensorDatalogs_REC vologs = new SensorDatalogs_REC();
                            vologs.RecordTimestamp = DateTime.Now;
                            vologs.DeviceID = "";
                            vologs.Node = Node;
                            vologs.Chanel = "ch1";
                            vologs.PresentValue = poPeakData.ToString();

                            List<Dictionary<string, string>> stl = new List<Dictionary<string, string>>();
                            Dictionary<string, string> st = new Dictionary<string, string>();
                            var builder = Builders<Sensor_REC>.Filter;
                            var filter = builder.And(builder.Eq<string>("Node", voSensor.Node), builder.Eq<string>("Chanel", voSensor.Chanel));
                            var resDatas = DataDevice.Find(filter).SingleOrDefault();
                            if (resDatas == null)
                            {
                                DataDevice.InsertOne(voSensor);
                                st.Add("id", voSensor.Node + "-" + voSensor.Chanel);
                                st.Add("device", "");
                                st.Add("max", "");
                                st.Add("min", "");
                            }
                            else
                            {
                                var update = Builders<Sensor_REC>.Update.Set("PresentValue", voSensor.PresentValue);
                                DataDevice.UpdateOne(filter, update);
                                st.Add("id", voSensor.Node + "-" + voSensor.Chanel + "-" + resDatas.Zone);
                                st.Add("device", resDatas.DeviceName);
                                st.Add("max", resDatas.Max.ToString());
                                st.Add("min", resDatas.Min.ToString());
                            }
                            // end insert and update logs
                            st.Add("value", voSensor.PresentValue);
                            stl.Add(st);

                            // publish to mqtt
                            string strValue = Convert.ToString(JsonConvert.SerializeObject(stl));
                            heandlerSignalR("WidgetRealtime", "SensorWidget", strValue);
                            //MqttClient.Publish("WidgetRealtime/SensorWidget", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                            DataLogs.InsertOne(vologs);

                            listNoiseVal[Node].Clear();
                        }
                    }
                }

            }
        }
        private void HandlerBasestationTwo()
        {

            try
            {
                string specialNode = "";
                string specialChanel = "";
                DateTime poDateTime = new DateTime();
                List<decimal> poVal = new List<decimal>();
                var DataDevice = database.GetCollection<Sensor_REC>("SensorDataDevice");
                var haystackPoint = database.GetCollection<HaystackPointView_REC>("HaystackPoint");
                Thread.Sleep(2000);
                while (true)
                {
                    //get all the data sweeps that have been collected, with a timeout of 500 milliseconds
                    mscl.DataSweeps sweeps = basestation2.getData(1000);

                    foreach (mscl.DataSweep sweep in sweeps)
                    {
                        sweep.nodeAddress();    //the node address the sweep is from
                        sweep.timestamp();      //the TimeStamp of the sweep
                        sweep.tick();           //the tick of the sweep (0 - 65535 counter)
                        sweep.sampleRate();     //the sample rate of the sweep
                        sweep.samplingType();   //the SamplingType of the sweep (sync, nonsync, burst, etc.)
                        sweep.nodeRssi();       //the signal strength at the Node
                        sweep.baseRssi();       //the signal strength at the BaseStation
                        sweep.frequency();      //the radio frequency this was collected on

                        //get the vector of data in the sweep
                        mscl.ChannelData data = sweep.data();
                        //iterate over each point in the sweep (one point per channel)
                        string nodeAddress = sweep.nodeAddress().ToString();
                        if (SpecialNode.IndexOf(nodeAddress) >= 0) { }
                        else // general node
                        {
                            Dictionary<int, Dictionary<string, string>> ResData = new Dictionary<int, Dictionary<string, string>>();
                            int i = 0;
                            foreach (mscl.WirelessDataPoint dataPoint in data)
                            {
                                List<Dictionary<string, string>> stl = new List<Dictionary<string, string>>();
                                Dictionary<string, string> ResDataObj = new Dictionary<string, string>();
                                dataPoint.storedAs();

                                // begin insert and update logs
                                Sensor_REC voSensor = new Sensor_REC();
                                voSensor.RecordTimestamp = DateTime.Now;
                                voSensor.DeviceID = basestation2.name();
                                voSensor.Node = sweep.nodeAddress().ToString();
                                voSensor.Chanel = dataPoint.channelName();
                                voSensor.PresentValue = dataPoint.as_string();

                                if (voSensor.Chanel.Length <= 4)
                                {
                                    Dictionary<string, string> st = new Dictionary<string, string>();
                                    var builder = Builders<Sensor_REC>.Filter;
                                    var filter = builder.And(builder.Eq<string>("Node", voSensor.Node), builder.Eq<string>("Chanel", voSensor.Chanel));
                                    var resDatas = DataDevice.Find(filter).SingleOrDefault();
                                    if (resDatas == null)
                                    {
                                        DataDevice.InsertOne(voSensor);
                                        st.Add("id", voSensor.Node + "-" + voSensor.Chanel);
                                        st.Add("device", "");
                                        st.Add("max", "");
                                        st.Add("min", "");
                                    }
                                    else
                                    {
                                        var update = Builders<Sensor_REC>.Update.Set("PresentValue", voSensor.PresentValue);
                                        DataDevice.UpdateOne(filter, update);
                                        st.Add("id", voSensor.Node + "-" + voSensor.Chanel + "-" + resDatas.Zone);
                                        st.Add("device", resDatas.DeviceName);
                                        st.Add("max", resDatas.Max.ToString());
                                        st.Add("min", resDatas.Min.ToString());

                                    }
                                    // end insert and update logs
                                    st.Add("value", voSensor.PresentValue);
                                    stl.Add(st);
                                    // publish to mqtt
                                    string strValue = Convert.ToString(JsonConvert.SerializeObject(stl));
                                    MqttClient.Publish("WidgetRealtime/SensorWidget", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                                    i++;
                                }
                            }
                        }
                    }

                    Thread.Sleep(interv);
                }
            }
            catch (mscl.Error e)
            {
                SendMail("the service sensor reported", e.Message);
                Dictionary<string, string> ResDataObj = new Dictionary<string, string>();
                ResDataObj.Add("msg", e.Message);
                string strValue = Convert.ToString(JsonConvert.SerializeObject(ResDataObj));
                MqttClient.Publish("Sensor/Lord/All/Error", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
        }

        #region mail sender
        private void SendMail(string subject, string message)
        {
            try
            {
                var mailData = database.GetCollection<MailServer_REC>("MailServer");
                var bd = Builders<MailServer_REC>.Filter.Empty;
                var voDt = mailData.Find(bd).SingleOrDefault();
                if (voDt != null)
                {
                    string from = voDt.emailAddress;
                    var fromAddr = new MailAddress(from, voDt.title);
                    string[] receivers = voDt.to.Split(',');
                    string[] receiversCC = voDt.cc.Split(',');
                    string[] receiversBCC = voDt.bcc.Split(',');
                    var toAddr = new MailAddress(receivers[0]);
                    var client = new SmtpClient
                    {
                        Host = voDt.host,
                        Port = voDt.port,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Timeout = 30 * 1000,
                        Credentials = new System.Net.NetworkCredential(fromAddr.Address, voDt.password)
                    };
                    using (var msg = new MailMessage(fromAddr, toAddr))
                    {
                        for (int i = 1; i <= receivers.Count() - 1; i++)
                        {
                            msg.To.Add(receivers[i]);
                        }
                        if (receiversCC[0] != "")
                        {
                            foreach (string adrCC in receiversCC)
                            {
                                msg.CC.Add(adrCC);
                            }
                        }
                        if (receiversBCC[0] != "")
                        {
                            foreach (string adrBCC in receiversBCC)
                            {
                                msg.Bcc.Add(adrBCC);
                            }
                        }
                        msg.Subject = subject;
                        msg.Body = message;
                        client.Send(msg);
                    }
                }
            }
            catch (Exception e)
            {
                Dictionary<string, string> ResDataObj = new Dictionary<string, string>();
                ResDataObj.Add("msg", e.Message);
                string strValue = Convert.ToString(JsonConvert.SerializeObject(ResDataObj));
                MqttClient.Publish("Sensor/Lord/All/Error", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }

        }
        #endregion

        #region utilities config
        private static void setConfig(string message)
        {
            object result = JsonConvert.DeserializeObject(message);
            JObject voobj = JObject.Parse(result.ToString());
            string basepatch = AppDomain.CurrentDomain.BaseDirectory + "AppSetting.json";

            Dictionary<string, string> voResData = new Dictionary<string, string>();
            voResData.Add("IPAddress", voobj["IPAddress"].ToString());
            voResData.Add("Port", voobj["Port"].ToString());
            voResData.Add("MQTTHOST", voobj["MQTTHOST"].ToString());
            voResData.Add("Interval", voobj["Interval"].ToString());

            // begin write to json file
            string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
            File.WriteAllText(basepatch, strValue);
            // end wriet to json file

            // publish mqtt
            MqttClient.Publish("Sensor/Lord/All/setConfigData", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            // Restart Apps
            Process currentProcess = Process.GetCurrentProcess();
            int pid = currentProcess.Id;
            string applicationName = currentProcess.ProcessName;
            RestartApp(pid, applicationName);
            System.Environment.Exit(1);
        }
        private static void getConfig(string message)
        {
            object result = JsonConvert.DeserializeObject(message);
            JObject voobj = JObject.Parse(result.ToString());

            string basepatch = AppDomain.CurrentDomain.BaseDirectory + "AppSetting.json";
            if (File.Exists(basepatch))
            {
                string Json = File.ReadAllText(basepatch);
                if (Json != "null")
                {
                    object rootresult = JsonConvert.DeserializeObject(Json);
                    JObject rootobj = JObject.Parse(rootresult.ToString());
                    string poIPAddress = rootobj["IPAddress"].ToString();
                    string poPort = rootobj["Port"].ToString();
                    string poMQTT = rootobj["MQTTHOST"].ToString();
                    string poIntv = rootobj["Interval"].ToString();
                    // publish mqtt
                    Dictionary<string, string> voResData = new Dictionary<string, string>();
                    voResData.Add("IPAddress", poIPAddress);
                    voResData.Add("Port", poPort);
                    voResData.Add("MQTTHOST", poMQTT);
                    voResData.Add("Interval", poIntv);
                    string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
                    MqttClient.Publish("Sensor/Lord/All/getConfigData", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                }
            }
        }
        protected override void OnStop()
        {
            Dictionary<string, string> voResData = new Dictionary<string, string>();
            voResData.Add("msg", "HASIOT - Sensor is stopped");
            string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
            MqttClient.Publish("Sensor/Lord/All/Error", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
        }
        public static bool IsValidJson(string strInput)
        {
            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(strInput);
                    return true;
                }
                catch (JsonReaderException jex)
                {
                    return false;
                }
                catch (Exception ex) //some other exception
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        static void RestartApp(int pid, string applicationName)
        {
            // Wait for the process to terminate
            Process process = null;
            try
            {
                process = Process.GetProcessById(pid);
                process.WaitForExit(1000);
            }
            catch (ArgumentException ex)
            {
                // ArgumentException to indicate that the 
                // process doesn't exist?   LAME!!
            }
            Process.Start(applicationName, "");
        }
        #endregion

        #region model
        public class Sensor_REC
        {
            [BsonId]
            public ObjectId _id { get; set; }
            public long RecordID { get; set; }
            public DateTime RecordTimestamp { get; set; }
            public int RecordStatus { get; set; }
            public string DeviceID { get; set; }
            public string DeviceName { get; set; }
            public string Node { get; set; }
            public string Chanel { get; set; }
            public string PresentValue { get; set; }
            public string Units { get; set; }
            public string Zone { get; set; }
            public double Min { get; set; }
            public double Max { get; set; }
        }
        public class SensorDatalogs_REC
        {
            [BsonId]
            public ObjectId _id { get; set; }
            public long RecordID { get; set; }
            public DateTime RecordTimestamp { get; set; }
            public int RecordStatus { get; set; }
            public string DeviceID { get; set; }
            public string DeviceName { get; set; }
            public string Node { get; set; }
            public string Chanel { get; set; }
            public string PresentValue { get; set; }
        }
        public class HaystackPointView_REC
        {
            [BsonId]
            public ObjectId _id { get; set; }
            public string id { get; set; }
            public string dis { get; set; }
            public bool point { get; set; }
            public string equipRef { get; set; }
            public string siteRef { get; set; }
            public string kind { get; set; }
            public string unit { get; set; }
            public int presentValue { get; set; }
            public string tz { get; set; }
            public int DeviceID { get; set; }
            public string DeviceName { get; set; }
            public string instanceID { get; set; }
            public bool sensor { get; set; }
            public string channelID { get; set; }
        }
        /*Curtain Constant*/
        public class ConfigurationModel
        {
            [BsonId]
            public ObjectId _id { get; set; }
            public decimal OutDoorTemperature { get; set; }

            public bool OutDoorTemperatureStatus { get; set; }

            public decimal ConstantLux { get; set; }

            public bool ConstantLuxStatus { get; set; }

            public decimal SkinTemperature { get; set; }

            public bool SkinTemperatureStatus { get; set; }

            public decimal HeartRate { get; set; }

            public bool HeartRateStatus { get; set; }

        }
        public class MailServer_REC
        {
            [BsonId]
            public ObjectId _id { get; set; }
            public DateTime recordTimestamp { get; set; }
            public int recordStatus { get; set; }
            public string emailAddress { get; set; }
            public string password { get; set; }
            public string title { get; set; }
            public string host { get; set; }
            public int port { get; set; }
            public string to { get; set; }
            public string cc { get; set; }
            public string bcc { get; set; }
        }
        #endregion
    }
}
